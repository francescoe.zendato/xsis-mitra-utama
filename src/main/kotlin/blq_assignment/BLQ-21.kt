package blq_assignment
//beware the manhole
fun main() {
    println("Masukkan lintasan, o = start, x = hole (ex. o___x______x): ")
    val lintasan = readln().trim().uppercase() + "BBB"

    if (lintasan.contains("XXX") || lintasan.contains("X_X_X") || lintasan[1] == 'X' || lintasan[2] == 'X') {
        println("FAILED")
    } else {
        var stamina = 0
        var distance = 0
        var output = ""

        while (stamina >= 0 && distance < lintasan.length - 3) {
            when (lintasan[distance + 1]) {
                'B' -> {
                    if (lintasan[distance] == '_') output += 'W'
                    stamina++
                    distance++
                    break
                }
                '_' -> {
                    output += 'W'
                    stamina++
                    distance++
                }
                'X' -> when (lintasan[distance + 2]) {
                    'B','X' -> {
                        output += 'J'
                        stamina -= 2
                        distance += 3
                    }
                    '_' -> when (lintasan[distance + 3]) {
                        'B','_' -> {
                            output += 'J'
                            stamina -= 2
                            distance += 3
                        }
                        'X' -> {
//                            if (lintasan[distance - 1])
                        }
                    }
                }
            }
        }

        if (stamina < 0) {
            println("FAILED")
        } else {
            println(output)
        }
    }
}
