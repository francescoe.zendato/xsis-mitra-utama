package blq_assignment
//konversi waktu am pm

fun main() {
    print("Input jam (ex. 11:55:07 atau 04:23 PM): ")
    var input = readln().replace(" ","").uppercase()
    var ampm = ""
    var jam = 0

    if (input.last() == 'M') {
        input = if (input.endsWith("AM")) {
            if (input.substring(0, 2).toInt() % 24 == 12) {
                "00" + input.substring(2,input.lastIndex-1)
            } else {
                input.substring(0,input.lastIndex-1)
            }
        } else {
            if (input.substring(0, 2).toInt() % 24 == 12) {
                input.substring(0,input.lastIndex-1)
            } else {
                (input.substring(0, 2).toInt() + 12).toString() + input.substring(2,input.lastIndex-1)
            }
        }
    } else {
        if (input.substring(0, 2).toInt() % 24 < 12) {
            ampm = "AM"
            if (input.substring(0, 2).toInt() % 24 == 0) {
                input = "12" + input.substring(2)
            }
        } else {
            ampm = "PM"
            if (input.substring(0, 2).toInt() % 24 != 12) {
                jam = input.substring(0, 2).toInt() - 12
                input = jam.toString().padStart(2,'0') + input.substring(2)
            }
        }
    }
    println("Hasil konversinya adalah $input $ampm")
}