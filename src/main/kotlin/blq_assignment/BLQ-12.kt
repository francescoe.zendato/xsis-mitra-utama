package blq_assignment
//sort manual

fun main() {
    print("Input daftar bilangan bulat yang ingin diurutkan (pisahkan dengan spasi): ")
    val input = readln().trim().split(" ").toTypedArray()
    val angka = Array(input.size){0}
    for (i in angka.indices) {
        angka[i] = input[i].toInt()
        if (i != 0) {
            var a = i-1
            var b = i
            while (angka[a] > angka[b]) {
                val x = angka[a]
                angka[a] = angka[b]
                angka[b] = x
                a--
                b--
                if (a < 0) break
            }
        }
    }
    for (i in angka.indices) print("${angka[i]} ")
}