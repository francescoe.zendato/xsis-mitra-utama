package blq_assignment
//alpha numeric string palindrome

fun main() {
    print("Masukkan kata/kalimat: ")
    val kata0 = readln().trim()

    //replace non-alphanumeric chars
    val nonAlphaNum = "[^a-zA-Z0-9]".toRegex()
    val kata = kata0.replace(nonAlphaNum,"").lowercase()
    println("Setelah menghapus karakter non-alphanumeric, diperoleh: ")
    println(kata)

    val pjg = kata.length
    var pal = "merupakan"
    for (i in 0..<(pjg/2)) {
        if (kata[i] != kata[pjg-i-1]) {
            pal = "bukan $pal"
            break
        }
    }
    print("$kata0 $pal palindrom.")
}