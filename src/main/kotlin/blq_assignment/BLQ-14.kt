package blq_assignment

import java.util.*

//rotasi bilangan

fun main() {
    print("Input barisan bilangan bulat, pisahkan dengan spasi (ex: 2 -1 45 100 3 0 77): ")
    val barisan = readln().trim().split(" ").toTypedArray()
    val n = barisan.size

    val mScan = Scanner(System.`in`)
    print("Input banyaknya rotasi: ")
    val rotasi = mScan.nextInt()

    println("Diperoleh barisan setelah $rotasi rotasi")

    for (i in 0..<n) {
        print(barisan[((i + rotasi) % n + n) % n]+" ")
    }
}