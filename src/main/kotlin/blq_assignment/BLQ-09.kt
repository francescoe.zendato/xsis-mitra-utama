package blq_assignment

import java.util.*

fun main() {
    val mScan = Scanner(System.`in`)
    print("Masukkan bilangan bulat non-negatif: ")
    val n = mScan.nextInt()

    if (n == 0) {
        println("n = 0 -> 0")
    } else {
        print("n = $n ->")
        for (i in 1..n) {
            print(" ${i*n}")
        }
    }
}