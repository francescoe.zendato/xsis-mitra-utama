package blq_assignment
//mean median modus

fun main() {
    print("Masukkan barisan bilangan yang akan dianalisis (pisahkan dengan spasi): ")
    val input = readln().trim().split(" ").toTypedArray()
    val angka = Array(input.size){0}
    for (i in input.indices) {
        angka[i] = input[i].toInt()
    }
    angka.sort()

    val maks = angka.size
    var median = angka[maks/2].toFloat()
    if (maks % 2 == 0) {
        median = (median + angka[maks/2 - 1])/2
    }

    var jumlah = 0

    for (i in angka.indices) {
        jumlah += angka[i]
    }

    val mean = jumlah.toFloat() / maks

    var modus = 0
    var modusCek = 0
    var nModus = 0
    var nModusCek = 0
    var i = 0
    var modusGanda = ""

    while (i < maks) {
        modusCek = angka[i]
        do {
            nModusCek++
            i++
            if (i >= maks) break
        } while (modusCek == angka[i])
        if (nModusCek > nModus) {
            modusGanda = ""
            nModus = nModusCek
            modus = modusCek
        } else if (nModusCek == nModus) { // untuk modus lebih dari satu
            modusGanda += "$modus, "
            nModus = nModusCek
            modus = modusCek
        }
        nModusCek = 0
    }

    print("Diperoleh mean = $mean, median = $median, modus = $modusGanda$modus (dengan frekuensi $nModus).")
}