package blq_assignment

import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

//parkir

fun main() {
    val lokalID = Locale("id","ID")
    val timeFormat = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss",lokalID)
    print("Input tanggal dan jam masuk (ex: 27 Januari 2019 05:00:01): ")
    val inp1 = readln()
    val masuk = LocalDateTime.parse(inp1,timeFormat)
    print("Input tanggal dan jam keluar (ex: 27 Januari 2019 17:45:03): ")
    val inp2 = readln()
    val keluar = LocalDateTime.parse(inp2,timeFormat)

    val durasi = Duration.between(masuk,keluar)
    val detik = durasi.toSeconds().toInt()
    var jam = durasi.toHours().toInt()
    if (detik % 3600 != 0) {
        jam++
    }
    var tarif0 = 0

    tarif0 += if (jam < 8) {
        jam
    } else if (jam < 24) {
        8
    } else if (jam < 32) {
        15 + jam - 24
    } else {
        23
    }

    val tarif = tarif0 * 1000
    print("Tarif parkir Anda sebesar $tarif rupiah.")
}