package blq_assignment

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

//pinjam buku

fun main() {
    val lokalID = Locale("id","ID")
    val timeFormat = DateTimeFormatter.ofPattern("dd MMMM yyyy",lokalID)

    print("Input tanggal pinjam (ex: 28 Februari 2016): ")
    val inp1 = readln()
    var pinjam = LocalDate.parse(inp1,timeFormat)
    print("Input tanggal kembali (ex: 07 Maret 2016): ")
    val inp2 = readln()
    val kembali = LocalDate.parse(inp2,timeFormat)
    println("Anda meminjam pada tanggal ${pinjam.format(timeFormat)} - ${kembali.format(timeFormat)}.")
    var durasi = 0
    var durasiDenda = 0

    while (pinjam < kembali) {
        durasi++
        pinjam = pinjam.plusDays(1)
    }

    if (durasi > 3) {
        durasiDenda += durasi - 3
        if (durasi > 7) {
            durasiDenda += 2 * (durasi - 7)
            if (durasi > 14) {
                durasiDenda += durasi - 14
            }
        }
    }

    val denda = durasiDenda * 100
    print("Anda meminjam selama $durasi hari, dengan denda $denda rupiah.")
}