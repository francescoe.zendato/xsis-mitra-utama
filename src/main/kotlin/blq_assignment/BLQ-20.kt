package blq_assignment
//gunting batu kertas

fun main() {
    var jarak = 0
    do {
        print("Input jarak antara player A dan player B: ")
        jarak = readln().trim().toInt()
    } while (jarak <= 0)

    val gbk = "[^GBKgbk]".toRegex()
    print("Input suitan player A (ex: GGGGBKBKG): ")
    val suitA = readln().replace(gbk," ").replace(" ","").uppercase()
    print("Input suitan player B: ")
    val suitB = readln().replace(gbk," ").replace(" ","").uppercase()

    val main = minOf(suitA.length,suitB.length)
    var i = 0
    var isDone = false
    while (i < main && jarak > 0 && !isDone) {
        if (suitA[i] != suitB[i]) {
            if (jarak == 1) {
                if ((suitA[i] == 'G' && suitB[i] == 'K')
                    || (suitA[i] == 'K' && suitB[i] == 'B')
                    || (suitA[i] == 'B' && suitB[i] == 'G')) {
                    println("A menang!")
                } else {
                    println("B menang!")
                }
                isDone = true
            } else {
                jarak--
            }
        }
        i++
    }
    if (!isDone) {
        println("Seri!")
    }
}