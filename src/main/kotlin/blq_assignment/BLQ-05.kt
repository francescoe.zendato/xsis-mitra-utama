package blq_assignment
//fibonacci

import java.util.*

fun main() {
    val mScan = Scanner(System.`in`)
    print("Masukkan panjang barisan Fibonacci: ")
    val n = mScan.nextInt()

    var a = 0
    var b = 1

    print("Barisan Fibonacci: ")
    for (i in 1..n) {
        val c = a + b
        if (i == 1) {
            print("1 ")
        } else {
            print("$c ")
            a = b
            b = c
        }
    }
}