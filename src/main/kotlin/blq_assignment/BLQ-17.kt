package blq_assignment
//ninja hattori gunung dan lembah

fun main() {
    print("Input perjalanan ninja Hattori (ex: NNTTNTNTNTNTTTNNTTNNNNNT): ")
    val perjalanan = readln().replace(" ","").uppercase()

    var posisi = 0
    var gunung = 0
    var lembah = 0

    for (i in perjalanan.indices) {
        when (perjalanan[i]) {
            'N' -> posisi++
            'T' -> posisi--
        }
        if (posisi == 0) {
            when (perjalanan[i]) {
                'N' -> lembah++
                'T' -> gunung++
            }
        }
    }

    println("Melewati gunung sebanyak $gunung kali.")
    print("Melewati lembah sebanyak $lembah kali.")
}