package blq_assignment
//donna makan kue terus olahraga

fun main() {
    print("Masukkan list jam saat Donna makan kue, pisahkan dengan spasi (ex: 9 13 15 17): ")
    val jam = readln().trim().split(" ").toTypedArray()

    print("Masukkan list kalori kue yang dimakan Donna berturur-turut, pisahkan dengan spasi (ex: 30 20 50 80): ")
    val kalori = readln().trim().split(" ").toTypedArray()

    print("Masukkan jam Donna mulai berolahraga (ex: 18): ")
    val hourStart = readln().trim().toInt()

    val irisan = minOf(jam.size,kalori.size)
    var time10 = 0F

    for (i in 0..< irisan) {
        val hour = jam[i].toInt()
        if (hour >= hourStart) {
            break
        } else {
            time10 += kalori[i].toLong() * (hourStart - hour) //satuan menit
        }
    }

    val cc = (time10 / 3) + 500
    println("Donna akan minum sebanyak $cc mL air sepanjang olahraga.")
}