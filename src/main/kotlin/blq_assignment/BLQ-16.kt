package blq_assignment
//alergi ikan

fun main() {
    println("Format pemesanan: nama_menu[spasi]harga[spasi]mengandung_ikan")
    println("Contoh: Tuna Sandwich 42K y, Spaghetti Carbonara 50K n, Pepes Lele Bumbu Kuning 32K y, Ayam Bakar Madu 45K n")
    print("Masukkan pesanan Anda: ")
    val pesananInput = readln().uppercase()
    val pesananNoSpasi = pesananInput.replace(" ","")
    val pesanan = pesananNoSpasi.split(",").toTypedArray()

    val strukInput = pesananInput.trim()
    val struk = strukInput.split(",").toTypedArray()

    println("---")
    for (i in struk.indices) {
        println(struk[i])
    }
    println("---")

    var fishMoney = 0F
    var noFishMoney = 0F

    for (p in pesanan) {
        var priceStr = ""
        for (i in p.lastIndex - 1 downTo 0) {
            if (p[i] in '0'..'9') {
                priceStr = "${p[i]}"
                for (j in i downTo 0) {
                    if (p[j] !in '0'..'9') {
                        priceStr = p.substring(j+1,i+1)
                        break
                    }
                }
                break
            }
        }

        when (p.last()) {
            'Y' -> fishMoney += priceStr.toInt()*1000F/3
            'N' -> {
                fishMoney += priceStr.toInt() * 250
                noFishMoney += priceStr.toInt() * 250
            }
        }
    }

    println("Anda membayar: ${(fishMoney*115/100).toInt()}.")
    println("Teman Anda yang alergi ikan membayar: ${(noFishMoney*115/100).toInt()}.")
}