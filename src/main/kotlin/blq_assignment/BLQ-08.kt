package blq_assignment
//min max 4 komponen barisan

fun main() {
    print("Masukkan barisan bilangan, pisahkan dengan spasi: ")
    val input0 = readln().trim()
    if (input0.isBlank()) return
    val input = input0.split(" ").toTypedArray()
    val sz = input.size
    var minStr = ""
    var maxStr = ""

    var min = 0
    var max = 0

    if (sz == 1) {
        min = input0.toInt()
        max = min
    } else if (sz <= 4) {
        for (i in input.indices) {
            min += input[i].toInt()
            minStr += if (i < sz - 1) {
                "${input[i]} + "
            } else {
                "${input[i]} "
            }
        }
        max = min
        maxStr = minStr
    } else {
        val angka = Array(sz) { 0 }
        for (i in input.indices) {
            angka[i] = input[i].toInt()
        }
        angka.sort()
        for (i in 0..3) {
            min += angka[i]
            max += angka[sz - i - 1]

            if (i < 3) {
                minStr += "${angka[i]} + "
                maxStr += "${angka[sz - i - 1]} + "
            } else {
                minStr += "${angka[i]} "
                maxStr += "${angka[sz - i - 1]} "
            }
        }
    }
    println("Diperoleh nilai minimumnya $minStr= $min dan maksimumnya $maxStr= $max.")
}