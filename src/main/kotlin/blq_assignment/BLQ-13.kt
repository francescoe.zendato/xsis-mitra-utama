package blq_assignment

import kotlin.math.abs

//sudut jarum jam

fun main() {
    print("Input jam (ex. 19:30): ")
    val input = readln().trim().split(":").toTypedArray()
    val menit = input[1].toFloat()
    val jam = input[0].toInt() % 12 + menit/60
    val selisih = abs(jam-menit/5)
    val degree = minOf(selisih,12-selisih) * 30
    println("Sudut yang dibentuk $degree derajat.")
}