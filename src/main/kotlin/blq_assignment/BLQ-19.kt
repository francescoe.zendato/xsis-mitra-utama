package blq_assignment
//pangram

fun main() {
    print("Masukkan string yang akan dicek pangram atau bukan: ")
    val input0 = readln().trim()
    val input = input0.replace(" ","").uppercase()
    var isPangram = true

    for (char in 'A'..'Z') {
        if (!input.contains(char)) {
            isPangram = false
            println("\"$input0\" bukan pangram karena tidak mengandung $char.")
            break
        }
    }

    if (isPangram) {
        println("\"$input0\" merupakan pangram.")
    }
}