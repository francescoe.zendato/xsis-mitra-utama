package blq_assignment
//barisan prima

import java.util.*

fun main() {
    val mScan = Scanner(System.`in`)
    print("Masukkan panjang barisan bilangan prima: ")
    val n = mScan.nextInt()
    var cekPrima = 3

    print("Barisan Bilangan Prima: ")

    for (i in 1..n) {
        if (i == 1) {
            print("2 ")
        } else {
            var ketemu = false
            while (!ketemu) {
                var isPrime = true
                var faktor = 2
                while (faktor * faktor <= cekPrima) {
                    if (cekPrima % faktor == 0) {
                        isPrime = false
                        break
                    } else {
                        faktor++
                    }
                }
                if (isPrime) {
                    print("$cekPrima ")
                    ketemu = true
                }
                cekPrima++
            }
        }
    }
}