package blq_assignment
//maksimal barang yang bisa dibeli

fun main() {
    println("Masukkan list item yang akan dibeli, pisahkan dengan spasi: ")
    val item = readln().trim().split(" ").toTypedArray()
    val harga = Array(item.size){""}

    for (i in item.indices) {
        println("Masukkan list harga untuk item ${item[i]}, pisahkan dengan spasi: ")
        harga[i] = readln().trim()
    }
}