package blq_assignment
//lilin fibonacci

fun main() {
    print("Masukkan panjang tiap lilin, pisahkan dengan spasi (ex: 3 3 9 6 7 8 23): ")
    val lilinStr = readln().trim().split(" ").toTypedArray()
    val n = lilinStr.size
    val waktu = Array(n){0F}

    var a = 0
    var b = 0

    for (i in lilinStr.indices) {
        val c = when (i) {
            0 -> 1
            else -> a + b
        }
        waktu[i] = lilinStr[i].toFloat()/c
        a = b
        b = c
    }

    var minTime = waktu[0]
    var output = "1"
    for (i in waktu.indices) {
        if (i > 0) {
            if (waktu[i] in 0F..<minTime) {
                output = "${1 + i}"
                minTime = waktu[i]
            } else if (waktu[i] == minTime) {
                output += ", ${1 + i}"
            }
        }
    }
    println("Setelah $minTime detik, lilin $output akan padam.")
}